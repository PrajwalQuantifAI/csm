import pandas as pd



path = ''
store = pd.HDFStore('%sdukas_ohlc.h5'%path)
#store9=pd.HDFStore('%sdukas_new_25.h5'%path)

keyss=store.keys()
currList=[]
data=pd.read_csv('EURUSD.csv',index_col='timestamp')
data=data[['Ask_Open','Ask_High','Ask_Low','Ask_Close','Ask_Open']]
data.columns=['Open','High','Low','Close','Volume']
data1=pd.read_csv('EURUSD.csv',index_col='timestamp')
data1=data1[['Ask_Open','Ask_High','Ask_Low','Ask_Close','Ask_Open']]
data1.columns=['Open','High','Low','Close','Volume']
#data.to_csv('testCSM.csv')
periods=[5,15,60,240]
for period in periods:
                data['diff%smin'%period]=0
                data['roundedDiff%smin'%period]=0
                
data=data[['diff%smin'%periods[0],'diff%smin'%periods[1],'diff%smin'%periods[2],'diff%smin'%periods[3]]].copy()
sumdf=data[['diff%smin'%periods[0],'diff%smin'%periods[1],'diff%smin'%periods[2],'diff%smin'%periods[3]]].copy()
for key in keyss:
        currList.append(key[6:9])
        currList.append(key[9:])
currencyList=list(set(currList))

currencyDict={}

for item in currencyList:
        currencyDict.update({item:data})

for key in keyss:
        self=pd.read_csv('%s.csv'%key[6:],index_col='timestamp')
        base=key[6:9]
        secondary=key[9:]
        #self['BaseCurr']=base
        #self['SecondaryCurr']=secondary
        
        
        df1=data.copy()
        df2=data.copy()

        dfb=currencyDict[base]
        dfs=currencyDict[secondary]
        
        if base=='JPY':
                pip=0.0100
        elif secondary=='JPY':
                pip=0.0100
        else:
                pip=0.0001
        
        self['Open']=self[['Bid_Open','Ask_Open']].mean(axis=1)
        self['Close']=self[['Bid_Close','Ask_Close']].mean(axis=1)
#        periods=[1,5,10,15]
        for period in periods:
                self['diff%smin'%period]=(self['Close'].shift(1)-self['Open'].shift(period+1))/pip
                self['negdiff%smin'%period]=-self['diff%smin'%period]
        dfp=self[['diff%smin'%periods[0],'diff%smin'%periods[1],'diff%smin'%periods[2],'diff%smin'%periods[3]]].copy()
        dfn=self[['negdiff%smin'%periods[0],'negdiff%smin'%periods[1],'negdiff%smin'%periods[2],'negdiff%smin'%periods[3]]].copy()
        dfn.columns=['diff%smin'%periods[0],'diff%smin'%periods[1],'diff%smin'%periods[2],'diff%smin'%periods[3]]
        
        
        
        
        df1=dfp+dfb
        
        
        currencyDict.update({base:df1})
        df2=dfn+dfs
        
        currencyDict.update({secondary:df2})
 
       
for key,value in currencyDict.items():
        df=value.copy()
        df1=df.abs()
        sumdf=df1+sumdf
        
csmDict={}
for key,value in currencyDict.items():
        df=value.copy()
        df=(df*100/sumdf)+50

        csmDict.update({key:df})